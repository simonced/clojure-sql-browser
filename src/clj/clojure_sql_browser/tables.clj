(ns clojure-sql-browser.tables
	(:require
		[clojure-sql-browser.layout :as layout]
		[clojure-sql-browser.db.core :as db]
		[clojure-sql-browser.db.settings :as db-settings]
		[clojure-sql-browser.helpers :as helpers]))


; =============================================
(defn -get-tables []
	(db/tables-listing {:schema db-settings/schema}))

; =============================================

(defn listing [request]
	"List tables in the schema"
	(layout/render request "tables_listing.html" {:tables (-get-tables)}))

(defn -table-type [table-name]
	"Returns :type from (-get-tables) for first table matching `table-name`."
	(->>
		(-get-tables)
		(filter #(= (:name %) table-name))
		first
		:type))

(defn -is-view? [table-name]
	"Checks if :type of table found in (-get-tables) of the name `table-name` is of type VIEW."
	(= "VIEW" (-table-type table-name)))

(defn url->orderby [query-params]
	"Builds order by clause from get parameters map.
	@key 'sort' the sorting column
	@key 'order' the sorting order (ASC or DESC)"
	(let [table-sort (get query-params "sort")
				table-order (get query-params "order")]
		(if table-sort
			(str table-sort " " table-order)
			(:order db-settings/default-query-parameters))))

(defn url->paging [query-params]
	"Builds limit clause from get parameters map.
	@param query-params 'page' : current page, default 1"
	(let [offset (-> (get query-params "page" 1) Integer. dec (#(* % (:limit db-settings/default-query-parameters))))]
		(db-settings/make-limit offset)))

; =============================================

(defn tables-url-handler [get-args]
	"Simply deals with url parameters.
	@param get-args are the current pages GET arguments
	GET arguments we expect: sort, order, page
	@return a function that will make a GET string"
	(fn [new-args]
		"@param new-args is a map with new arguments to make the url from"
		(helpers/build-url-with-get-params (merge get-args new-args))))

(defn content [{:keys [path-params query-params] :as request}]
	"Display table content"
	(let [table-name (:table-name path-params)
				table-orderby (url->orderby query-params)
				table-limit (url->paging query-params)
				sql-parameters (assoc db-settings/default-query-parameters
													 :table table-name
													 :order table-orderby
													 :limit table-limit)
				table-content (db/table-content sql-parameters)
				current-page (-> (get query-params "page" "1") Integer.)
				total-rows (if (-is-view? table-name) 0 (:total (db/total-rows {:table table-name})))
				total-pages (->
											(/
												total-rows
												(:limit db-settings/default-query-parameters))
											Math/floor
											int
											(max 1))]
		(layout/render request "table_content.html"
									 {:url-handler    (tables-url-handler query-params)
										:table-is-view? (-is-view? table-name)
										:table-name     table-name
										:table-orderby  table-orderby
										:table-headers  (keys (first table-content))       ; TODO fetch columns in a different way to get their details
										:table-content  (map vals table-content)
										:total-rows     total-rows
										:paging-current current-page
										:paging-total   total-pages})))