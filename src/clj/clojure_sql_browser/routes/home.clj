(ns clojure-sql-browser.routes.home
  (:require
   [clojure-sql-browser.layout :as layout]
   [clojure.java.io :as io]
   [clojure-sql-browser.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]
   [clojure-sql-browser.pages :as pages]
   [clojure-sql-browser.tables :as tl]))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get tl/listing}]                                  ;; change for connection listing (once multiple connections supported)
   ["/browse/db/{table-name}" {:get tl/content}]            ;; change /db/ for the name of the current schema?
   ["/about" {:get home-page}]
   ; trying to deal with dynamic pages
   ["/pages/:page-name" {:get pages/handle}]
   ])

