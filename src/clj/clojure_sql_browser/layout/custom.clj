(ns clojure-sql-browser.layout.custom
  (:require [selmer.filters :as filters]
             [selmer.parser :as parser]
             [clojure.string :as str]))

;;; Icons and visual representation

(defn -table-type->icon
  "Returns the icon corresponding to the type of the table (VIEW or not)."
  [table-type]
  (cond (= table-type "VIEW") "<i class='fa fa-eye'></i>"
         :else "<i class='fa fa-table'></i>"))

(defn -symbol->text
  "Removes : in from of symbols."
  [symbol]
  (str/replace symbol ":" ""))

;;; Sorting links

(defn -make-sort-link
  "Generates one sort link (ASC or DESC)"
  [url-handler column-name sort link-class]
  (let [icon-class (if (= "ASC" sort) "fa fa-chevron-up" "fa fa-chevron-down")
        url (url-handler {"sort" column-name "order" sort})]
     (format "<a href='%s' class='sort-btn %s'><i class='%s'></i></a>" url link-class icon-class)))

(defn -make-sortable-header
  "Generates table header sort links + the title text.
	string @key content :sortable-header :content for header content
	string @key context-map :table-orderby the current order by used in query to display the page"
  [_args context-map content]
  (let [column-name (-> content :sortable-header :content -symbol->text str/trim)
        current-sort (:table-orderby context-map)
        url-handler (:url-handler context-map)
        link-class-asc (if (= current-sort (str column-name " ASC")) "active" "")
        link-class-desc (if (= current-sort (str column-name " DESC")) "active" "")]
     (str
      (-make-sort-link url-handler column-name "ASC" link-class-asc)
      "<span>" column-name "</span>"
      (-make-sort-link url-handler column-name "DESC" link-class-desc))))

;;; Paging

(def paging-pading 2)

(defn -merge-2-chunks
  "Makes 2 chunks one.
  Result doesn't contain duplicates and is sorted."
  [a b]
  (->> (list a b)
       flatten
       set
       sort))

(defn -merge-pages-chunks
  "Merging paging chunks if they are close enough.
  Merging from left to right."
  [left next & others]
  (if (>= (- (first next) (last left)) 2)
    ;; "don't merge"
    (if others
      (apply list left (apply -merge-pages-chunks next others))
      (list left next))
    ;; merge
    (if others
      (apply -merge-pages-chunks (-merge-2-chunks left next) others)
      (list (-merge-2-chunks left next)))
    )
  )

(defn -generate-pages-chunks
  "Generate the pages chunks from total number of page and current page."
  [total-pages current-page]
  (let [chunk-left (range 1 (+ 1 paging-pading 1))
        chunk-middle (range (- current-page paging-pading) (+ current-page paging-pading 1))
        chunk-right (range (- total-pages paging-pading) (inc total-pages))
        keep-in-boundaries #(and (>= % 1) (<= % total-pages))]
  	(->> (-merge-pages-chunks chunk-left chunk-middle chunk-right)
         (map #(filter keep-in-boundaries %)))))

(defn -make-page-link
  "Generates a paging link (one page)"
  [url-handler url]
  (format "<a href='%s'>%s</a>" (url-handler {"page" url}) url))

(defn -make-paging-element [url-handler current page]
  (cond (= page current) (format "<span class='current'>%s</span>" page)
        (= page \-) "<span>...</span>"
        :else (-make-page-link url-handler page)))

(defn -make-paging
  "Builds paging links.
   @key context-map :url-handler
   @key context-map :paging-current current page
   @key context-map :total-pages the total number of pages"
   [_args context-map]
  (let [url-handler (:url-handler context-map)
        current-page (:paging-current context-map)
        total-pages (:paging-total context-map)
        chunks (-generate-pages-chunks total-pages current-page)
        ]
    (->> chunks
         (reduce #(flatten (list %1 \- %2)))
         (map (partial -make-paging-element url-handler current-page))
         (str/join "&nbsp;&nbsp;&nbsp;"))))

;;; Filters and Tags

(filters/add-filter! :symbol->text -symbol->text)
(filters/add-filter! :format-number #(format "%,d" %))
(filters/add-filter! :table-type->icon -table-type->icon)
(parser/add-tag! :sortable-header -make-sortable-header :endsortable-header)
(parser/add-tag! :paging -make-paging)

