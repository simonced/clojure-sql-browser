(ns clojure-sql-browser.helpers)


(defn build-url-with-get-params [get-params]
	(->> get-params
			 (map (fn [[k v]] (format "%s=%s" k v)))
			 (#(clojure.string/join "&" %))
			 (str "?")))
(comment
	(build-url-with-get-params {"page" 2 "sort" "ASC"})
	)