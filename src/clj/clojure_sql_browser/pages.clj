(ns clojure-sql-browser.pages
	(:require
		[clojure.java.io :as io]
		[clojure-sql-browser.layout :as layout]
		))

;; Settings
(def resources-folder "./resources/html/")
(def pages-folder "/pages/")

;; custom filters
(selmer.filters/add-filter! :format-decimals #(format "%,d" %))

(defn -make-file-path [page-name]
	(format "%s%s.html" pages-folder page-name))

(defn -page-exists [path]
	(.exists (io/file (str resources-folder path))))

(defn handle [{:keys [path-params] :as request}]
	;(clojure.pprint/pprint request)
	(let [page-name (:page-name path-params)
				page-file (-make-file-path page-name)]
		(if (-page-exists page-file)
			(layout/render request page-file)
			(layout/error-page {:status 404 :title "Error from <Pages>" :message "File not found!"}))
		)
	)

