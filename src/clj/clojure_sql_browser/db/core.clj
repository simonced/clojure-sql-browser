(ns clojure-sql-browser.db.core
	(:require
		[clojure-sql-browser.config :refer [env]]
		[conman.core :as conman]
		[mount.core :refer [defstate]]))


(defstate ^:dynamic *db*
					:start (conman/connect!
									 {:jdbc-url (env :database-url)})
					:stop (conman/disconnect! *db*))

(conman/bind-connection *db* "clojure_sql_browser/db/sql/queries.sql")