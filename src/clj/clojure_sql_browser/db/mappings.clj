(ns clojure-sql-browser.db.mappings
	(:require
		[clojure.edn :as edn]
		[clojure-sql-browser.db.settings :as settings]))

(defn mappings []
	"read default mapping file.
	(!) uses the default schema name. (defined in settings)"
	(->
		(edn/read-string (slurp settings/mappings-file))
		(get settings/schema)))

(defn has-mappings? [table column]
	"Checks if a table has some mappings"
	(let [table-column (str table "." column)]
		(contains?
			(->> (mappings)
					 :column-relation
					 (into [])
					 flatten
					 set)
			table-column)))
