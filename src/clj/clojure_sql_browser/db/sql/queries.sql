-- :name tables-listing :? :*
-- :quoting :mysql
SELECT
 TABLE_NAME as name,
 TABLE_TYPE as type
FROM information_schema.`TABLES`
WHERE TABLE_SCHEMA = :schema;

-- :name table-content :? :*
-- :doc Simply list table content
-- :quoting :mysql
SELECT * FROM :i:table
ORDER BY :i:order
LIMIT :i:limit;

-- :name total-rows :? :1
-- :doc Will return the number of rows in a table
-- :quoting :mysql
-- SELECT COUNT(*) AS total FROM :i:table;
SELECT TABLE_ROWS AS total
FROM information_schema.`TABLES`
WHERE TABLE_NAME = :table;