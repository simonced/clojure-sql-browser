(ns clojure-sql-browser.db.settings
	(:require [clojure.string :as str]))

;; TODO make that dynamic?
(def schema "employees")
(def mappings-file "schema_mappings.edn")

(def default-query-parameters
	{:order "NULL"
	 :limit 100
	 :offset 0})

(defn make-limit [offset]
	"Generates the limt clause for SQL query.
	@param int offset (starting from 0)"
	(if (> offset 0)
		(str offset ", " (:limit default-query-parameters))
		(str (:limit default-query-parameters))))