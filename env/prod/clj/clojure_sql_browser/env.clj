(ns clojure-sql-browser.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[clojure-sql-browser started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[clojure-sql-browser has shut down successfully]=-"))
   :middleware identity})
