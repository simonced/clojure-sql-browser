(ns clojure-sql-browser.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [clojure-sql-browser.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[clojure-sql-browser started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[clojure-sql-browser has shut down successfully]=-"))
   :middleware wrap-dev})
