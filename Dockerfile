FROM openjdk:8-alpine

COPY target/uberjar/clojure-sql-browser.jar /clojure-sql-browser/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/clojure-sql-browser/app.jar"]
